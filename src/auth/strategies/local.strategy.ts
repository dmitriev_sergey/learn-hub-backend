import { HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { ErrorMessages } from 'src/constants/errorMessages';
import { AuthService } from '../auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private authService: AuthService) {
        super({
            usernameField: 'email',
            passwordField: 'password',
        });
    }

    async validate(username: string, password: string): Promise<any> {
        const user = await this.authService.validateUser(username, password);
        if (!user) {
						console.error('#local.strategy, validation error: incorrect username or password');
            throw new UnauthorizedException({
                status: HttpStatus.UNAUTHORIZED,
                error: ErrorMessages.emailOrPassWrong,
            });
        }
				console.log('#local.strategy, validation successful: username and password are correct');
        return user;
    }
}
