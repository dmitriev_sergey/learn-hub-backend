export interface IJwtPayload {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    user_role: number;
}
