import { HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';

import { Users } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { IRegistrationStatus } from './interfaces/registrationStatus.interface';
import { IJwtPayload } from './interfaces/jwtPayload.interface';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { TokensService } from 'src/tokens/tokens.service';
import { UserDto } from 'src/users/dto/user-dto';

@Injectable()
export class AuthService {
    constructor(
        private readonly usersService: UsersService,
        private readonly tokensService: TokensService,
    ) {}

    async register(user: CreateUserDto) {
        let status: IRegistrationStatus = {
            success: true,
            message: 'user registered',
        };

        try {
            const userInfo = await this.usersService.register(user);
            const tokens = this.tokensService.generateTokens(userInfo);
            await this.tokensService.saveRefreshToken({
                userId: userInfo.id,
                token: tokens.refreshToken,
            });
            return { user: userInfo, tokens, status };
        } catch (error) {
            status = {
                success: false,
                message: error.message,
            };
            return { status };
        }
    }

    async logout(refreshToken: string) {
        await this.tokensService.deleteRefreshToken(refreshToken);
    }

    async refresh(refreshToken: string) {
        const userData = await this.tokensService.validateRefreshToken(
            refreshToken,
        );
        const refreshTokenDB = await this.tokensService.findRefreshToken(
            refreshToken,
        );

        if (!userData || !refreshTokenDB) {
            throw new UnauthorizedException({
                status: HttpStatus.UNAUTHORIZED,
                message: 'User is not authorized',
            });
        }

        const user = new UserDto(userData);
        const tokens = this.tokensService.generateTokens(user);
        await this.tokensService.saveRefreshToken({
            userId: user.id,
            token: tokens.refreshToken,
        });
        return { user, tokens };
    }

    async validateUserToken(payload: IJwtPayload): Promise<Users> {
        return await this.usersService.findOne(payload.id);
    }

    async validateUser(email: string, password: string): Promise<UserDto> {
        const userData = await this.usersService.findByEmail(email);
        const match = await userData.comparePassword(password);
        if (userData && match) {
            const user = new UserDto(userData);
            return user;
        }

        return null;
    }
}
