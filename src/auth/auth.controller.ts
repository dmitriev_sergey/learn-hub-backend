import {
    Body,
    Controller,
    Get,
    HttpStatus,
    Post,
    Request,
    Response,
    UnauthorizedException,
    UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { REFRESH_TOKEN_EXPIRATION } from 'src/constants/tokens';
import { TokensService } from 'src/tokens/tokens.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UserDto } from 'src/users/dto/user-dto';
import { UsersService } from 'src/users/users.service';
import { AuthService } from './auth.service';
import { LoginUserDto } from './dto/loginUser.dto';

@Controller('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService,
        private readonly usersService: UsersService,
        private readonly tokensService: TokensService,
    ) {}

    @Post('register')
    async register(@Body() createUserDto: CreateUserDto, @Response() res) {
        console.log('should register, dto:', createUserDto);
        const result = await this.authService.register(createUserDto);
        if (!result.status.success) {
            return res.status(HttpStatus.BAD_REQUEST).json(result);
        }
        res.cookie('refreshToken', result.tokens.refreshToken, {
            maxAge: REFRESH_TOKEN_EXPIRATION,
        });
        return res.status(HttpStatus.OK).json(result);
    }

    @UseGuards(AuthGuard('local'))
    @Post('login')
    public async login(@Response() res, @Body() login: LoginUserDto) {
        const userData = await this.usersService.findByEmail(login.email);
        const user = new UserDto(userData);
        if (!userData) {
            res.status(HttpStatus.NOT_FOUND).json({
                message: 'User not found',
            });
        } else {
            const tokens = this.tokensService.generateTokens(user);
            await this.tokensService.saveRefreshToken({
                userId: +user.id,
                token: tokens.refreshToken,
            });
            res.cookie('refreshToken', tokens.refreshToken, {
                maxAge: REFRESH_TOKEN_EXPIRATION,
            });
            res.status(HttpStatus.OK).json({ tokens, user });
        }
    }

    @Get('logout')
    public async logout(@Request() request, @Response() res) {
        const { refreshToken } = request.cookies;
        await this.authService.logout(refreshToken);
        res.clearCookie('refreshToken');
        res.status(HttpStatus.OK).json({ message: 'You have been logged out' });
    }

    @Get('refresh')
    public async refresh(@Request() request, @Response() res) {
        const { refreshToken } = request.cookies;
        if (!refreshToken) {
						console.error('#auth.controller, refresh error: No refresh token provided')
            throw new UnauthorizedException({
                status: HttpStatus.UNAUTHORIZED,
                message: 'User is not authorized',
            });
        }
        const result = await this.authService.refresh(refreshToken);
				console.log('#auth.controller, refresh success: new tokens generated');
				
        res.cookie('refreshToken', result.tokens.refreshToken, {
            maxAge: REFRESH_TOKEN_EXPIRATION,
        });
        res.status(HttpStatus.OK).json({ ...result });
    }
}
