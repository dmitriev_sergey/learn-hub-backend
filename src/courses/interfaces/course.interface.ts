export interface ICourse {
    id: string;
    title: string;
    description: string;
    creator_id: number;
}
