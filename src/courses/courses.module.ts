import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CoursesService } from './courses.service';
import { CoursesController } from './courses.controller';
import { Courses } from './course.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Courses])],
    providers: [CoursesService],
    controllers: [CoursesController],
})
export class CoursesModule {}
