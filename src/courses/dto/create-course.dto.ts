import { IsString, IsInt, IsNotEmpty } from 'class-validator';

export class CreateCourseDto {
    @IsString()
    @IsNotEmpty()
    readonly title: string;

    @IsString()
    @IsNotEmpty()
    readonly description: string;

    @IsInt()
    @IsNotEmpty()
    readonly creatorId: number;

		@IsInt()
    @IsNotEmpty()
    readonly order: number;
}
