import { Modules } from 'src/modules/module.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

@Entity()
export class Courses {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    creator_id: number;

    @OneToMany(
        () => Modules,
        modules => modules.course,
    )
    modules: Modules[];

		@Column()
    order: number;
}
