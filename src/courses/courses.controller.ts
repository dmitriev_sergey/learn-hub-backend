import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    Query,
    UseGuards,
} from '@nestjs/common';
import { DeleteResult } from 'typeorm';
import { Courses } from './course.entity';
import { CoursesService } from './courses.service';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('courses')
@UseGuards(AuthGuard('jwt'))
export class CoursesController {
    constructor(private readonly coursesService: CoursesService) {}

    @Get()
    findAll(@Query() query): Promise<Courses[]> {
        return this.coursesService.findAll(query);
    }

    @Get(':id')
    findOne(@Param() params): Promise<Courses> {
        return this.coursesService.findOne(params.id);
    }

    @Post()
    createOne(@Body() createCourseDto: CreateCourseDto): Promise<Courses> {
        return this.coursesService.createCourse(createCourseDto);
    }

    @Delete(':id')
    deleteOne(@Param() params): Promise<DeleteResult> {
        return this.coursesService.deleteCourse(params.id);
    }

    @Patch(':id')
    updateOne(
        @Param() params,
        @Body() updateCourseDto: UpdateCourseDto,
    ): Promise<Courses> {
        return this.coursesService.updateCourse(params.id, updateCourseDto);
    }
}
