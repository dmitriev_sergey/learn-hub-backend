import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Equal, IsNull, Like, Not, Repository } from 'typeorm';

import { Courses } from './course.entity';
import { getSkipCount } from '../utils/selection';
import { DEFAULT_PAGE, DEFAULT_SIZE } from '../constants/common';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';

@Injectable()
export class CoursesService {
    constructor(
        @InjectRepository(Courses)
        private readonly courseRepository: Repository<Courses>,
    ) {}

    async findAll(query): Promise<Courses[]> {
        const {
            page = DEFAULT_PAGE,
            size = DEFAULT_SIZE,
            search = '',
            creator,
        } = query;
        const skip = getSkipCount(page, size);
        const creator_id = creator ? Equal(creator) : Not(IsNull());
        return await this.courseRepository.find({
            where: {
                title: Like(`%${search}%`),
                creator_id,
            },
            skip,
            take: size,
        });
    }

    async findOne(id: number): Promise<Courses> {
        return await this.courseRepository.findOne(id);
    }

    async createCourse(createCourseDto: CreateCourseDto): Promise<Courses> {
        const body = {
            title: createCourseDto.title,
            description: createCourseDto.description || null,
            creator_id: createCourseDto.creatorId,
						order: createCourseDto.order
        };
        try {
            return await this.courseRepository.save(body);
        } catch (err) {
            console.error(
                'Course was not created, error message:',
                err.message,
            );
        }
    }

    async deleteCourse(id: number): Promise<DeleteResult> {
        const course = await this.courseRepository.findOne(id);
        return await this.courseRepository.delete(course);
    }

    async updateCourse(
        id: number,
        updateCourseDto: UpdateCourseDto,
    ): Promise<Courses> {
        const course = await this.courseRepository.findOneOrFail(id);

        if (!course) {
            console.log('Course was not found');
        }

        const body = {
            title: updateCourseDto.title,
            description: updateCourseDto.description || null,
            creator_id: updateCourseDto.creatorId,
						order: updateCourseDto.order
        };

        await this.courseRepository.update(id, body);
        return this.courseRepository.findOne(id);
    }
}
