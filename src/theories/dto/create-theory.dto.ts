import { IsString, IsInt, IsNotEmpty } from 'class-validator';

export class CreateTheoryDto {
    @IsString()
    @IsNotEmpty()
    readonly type: string;

    @IsString()
    @IsNotEmpty()
    readonly title: string;

    @IsString()
    @IsNotEmpty()
    readonly description: string;

    @IsInt()
    @IsNotEmpty()
    readonly moduleId: number;

    @IsString()
    @IsNotEmpty()
    readonly data: string;

    @IsInt()
    @IsNotEmpty()
    readonly order: number;
}
