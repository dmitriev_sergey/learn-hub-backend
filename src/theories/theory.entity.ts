import { Modules } from 'src/modules/module.entity';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

@Entity()
export class Theories {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    type: string;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column({ name: 'module_id' })
    moduleId: number;

    @Column()
    data: string;

    @ManyToOne(
        () => Modules,
        modules => modules.theories,
    )
    modules: Modules;

    @Column()
    order: number;
}
