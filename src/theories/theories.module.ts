import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TheoriesController } from './theories.controller';
import { TheoriesService } from './theories.service';
import { Theories } from './theory.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Theories])],
    controllers: [TheoriesController],
    providers: [TheoriesService],
})
export class TheoriesModule {}
