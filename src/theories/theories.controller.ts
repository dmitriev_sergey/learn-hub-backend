import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    Query,
} from '@nestjs/common';
import { DeleteResult } from 'typeorm';

import { CreateTheoryDto } from './dto/create-theory.dto';
import { TheoriesService } from './theories.service';
import { Theories } from './theory.entity';
import { UpdateTheoryDto } from './dto/update-theory.dto';

@Controller('theories')
export class TheoriesController {
    constructor(private readonly theoriesService: TheoriesService) {}

    @Get()
    findAll(@Query() query): Promise<Theories[]> {
        return this.theoriesService.findAll(query);
    }

    @Get(':id')
    findOne(@Param() params): Promise<Theories> {
        return this.theoriesService.findOne(params.id);
    }

    @Post()
    createOne(@Body() createTheoryDto: CreateTheoryDto): Promise<Theories> {
        return this.theoriesService.createTheory(createTheoryDto);
    }

    @Delete(':id')
    deleteOne(@Param() params): Promise<DeleteResult> {
        return this.theoriesService.deleteTheory(params.id);
    }

    @Patch(':id')
    updateOne(
        @Param() params,
        @Body() updateTheoryDto: UpdateTheoryDto,
    ): Promise<Theories> {
        return this.theoriesService.updateTheory(params.id, updateTheoryDto);
    }
}
