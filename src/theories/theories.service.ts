import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Equal, IsNull, Like, Not, Repository } from 'typeorm';

import { getSkipCount } from '../utils/selection';
import { DEFAULT_PAGE, DEFAULT_SIZE } from '../constants/common';
import { CreateTheoryDto } from './dto/create-theory.dto';
import { UpdateTheoryDto } from './dto/update-theory.dto';
import { Theories } from './theory.entity';

@Injectable()
export class TheoriesService {
    constructor(
        @InjectRepository(Theories)
        private readonly theoriesRepository: Repository<Theories>,
    ) {}

    async findAll(query): Promise<Theories[]> {
        const {
            page = DEFAULT_PAGE,
            size = DEFAULT_SIZE,
            search = '',
            module,
        } = query;
        const skip = getSkipCount(page, size);
        const module_id = module ? Equal(module) : Not(IsNull());
        return await this.theoriesRepository.find({
            where: {
                title: Like(`%${search}%`),
                module_id,
            },
            skip,
            take: size,
        });
    }

    async findOne(id: number): Promise<Theories> {
        return await this.theoriesRepository.findOne(id);
    }

    async createTheory(createTheoryDto: CreateTheoryDto): Promise<Theories> {
        try {
            const body = {
                type: createTheoryDto.type,
                data: createTheoryDto.data,
                title: createTheoryDto.title,
                description: createTheoryDto.description,
                moduleId: createTheoryDto.moduleId,
                order: createTheoryDto.order,
            };
            return await this.theoriesRepository.save(body);
        } catch (err) {
            console.error(
                'Theory was not created, error message:',
                err.message,
            );
        }
    }

    async deleteTheory(id: number): Promise<DeleteResult> {
        const course = await this.theoriesRepository.findOne(id);
        return await this.theoriesRepository.delete(course);
    }

    async updateTheory(
        id: number,
        updateTheoryDto: UpdateTheoryDto,
    ): Promise<Theories> {
        const theory = await this.theoriesRepository.findOneOrFail(id);

        if (!theory) {
            console.log('Theory was not found');
        }

        const body = {
            type: updateTheoryDto.type,
            title: updateTheoryDto.title,
            description: updateTheoryDto.description || null,
            moduleId: updateTheoryDto.moduleId,
            data: updateTheoryDto.data,
        };

        await this.theoriesRepository.update(id, body);
        return this.theoriesRepository.findOne(id);
    }
}
