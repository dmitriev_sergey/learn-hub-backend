import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { ModulesModule } from './modules/modules.module';
import { CoursesModule } from './courses/courses.module';
import { TheoriesModule } from './theories/theories.module';
import { UsersModule } from './users/users.module';
import { TestsModule } from './tests/tests.module';
import { AuthModule } from './auth/auth.module';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { QuestionsModule } from './questions/questions.module';
import { TokensModule } from './tokens/tokens.module';

@Module({
    imports: [
        ConfigModule.forRoot(),
        TypeOrmModule.forRoot({
            type: 'postgres',
            host: process.env.DB_HOST,
            port: +process.env.DB_PORT,
            username: process.env.DB_USERNAME,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_NAME,
            entities: ['dist/**/*.entity{.ts,.js}'],
            namingStrategy: new SnakeNamingStrategy(),
            synchronize: true,
        }),
        CoursesModule,
        ModulesModule,
        TheoriesModule,
        UsersModule,
        TestsModule,
        AuthModule,
        QuestionsModule,
        TokensModule,
    ],
})
export class AppModule {}
