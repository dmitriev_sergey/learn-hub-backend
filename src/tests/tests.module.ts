import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tests } from './test.entity';
import { TestsController } from './tests.controller';
import { TestsService } from './tests.service';

@Module({
    imports: [TypeOrmModule.forFeature([Tests])],
    controllers: [TestsController],
    providers: [TestsService],
})
export class TestsModule {}
