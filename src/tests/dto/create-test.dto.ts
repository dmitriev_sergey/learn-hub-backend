import { IsInt, IsNotEmpty, IsString } from 'class-validator';

export class CreateTestDto {
    @IsString()
    @IsNotEmpty()
    readonly type: string;

    @IsString()
    @IsNotEmpty()
    readonly title: string;

    @IsString()
    @IsNotEmpty()
    readonly description: string;

    @IsInt()
    @IsNotEmpty()
    readonly moduleId: number;

    @IsInt()
    @IsNotEmpty()
    readonly order: number;
}
