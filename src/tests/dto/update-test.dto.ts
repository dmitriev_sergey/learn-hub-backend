import { PartialType } from '@nestjs/mapped-types';
import { CreateTestDto } from './create-test.dto';

export class UpdateTheoryDto extends PartialType(CreateTestDto) {}
