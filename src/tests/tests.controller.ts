import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    Query,
} from '@nestjs/common';
import { DeleteResult } from 'typeorm';

import { CreateTestDto } from './dto/create-test.dto';
import { TestsService } from './tests.service';
import { Tests } from './test.entity';

@Controller('tests')
export class TestsController {
    constructor(private readonly testsService: TestsService) {}

    @Get()
    findAll(@Query() query): Promise<Tests[]> {
        return this.testsService.findAll(query);
    }

    @Get(':id')
    findOne(@Param() params): Promise<Tests> {
        return this.testsService.findOne(params.id);
    }

    @Post()
    createOne(@Body() createTestDto: CreateTestDto): Promise<Tests> {
        return this.testsService.createTest(createTestDto);
    }

    @Delete(':id')
    deleteOne(@Param() params): Promise<DeleteResult> {
        return this.testsService.deleteTest(params.id);
    }

    @Patch(':id')
    updateOne(
        @Param() params,
        @Body() createTestDto: CreateTestDto,
    ): Promise<Tests> {
        return this.testsService.updateTest(params.id, createTestDto);
    }
}
