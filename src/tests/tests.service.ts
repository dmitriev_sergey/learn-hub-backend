import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Equal, IsNull, Like, Not, Repository } from 'typeorm';

import { getSkipCount } from '../utils/selection';
import { DEFAULT_PAGE, DEFAULT_SIZE } from '../constants/common';
import { CreateTestDto } from './dto/create-test.dto';
import { Tests } from './test.entity';

@Injectable()
export class TestsService {
    constructor(
        @InjectRepository(Tests)
        private readonly TestsRepository: Repository<Tests>,
    ) {}

    async findAll(query): Promise<Tests[]> {
        const {
            page = DEFAULT_PAGE,
            size = DEFAULT_SIZE,
            search = '',
            module,
        } = query;
        const skip = getSkipCount(page, size);
        const module_id = module ? Equal(module) : Not(IsNull());
        return await this.TestsRepository.find({
            where: {
                title: Like(`%${search}%`),
                module_id,
            },
            skip,
            take: size,
        });
    }

    async findOne(id: number): Promise<Tests> {
        return await this.TestsRepository.findOne(id);
    }

    async createTest(createTestDto: CreateTestDto): Promise<Tests> {
        const body = {
            type: createTestDto.type,
            title: createTestDto.title,
            description: createTestDto.description || null,
            moduleId: createTestDto.moduleId,
            order: createTestDto.order,
        };
        try {
            return await this.TestsRepository.save(body);
        } catch (err) {
            console.error('Test was not created, error message:', err.message);
        }
    }

    async deleteTest(id: number): Promise<DeleteResult> {
        const course = await this.TestsRepository.findOne(id);
        return await this.TestsRepository.delete(course);
    }

    async updateTest(id: number, newValue: CreateTestDto): Promise<Tests> {
        const course = await this.TestsRepository.findOneOrFail(id);
        if (!course) {
            console.log('Test was not found');
        }

        const body = {
            type: newValue.type,
            title: newValue.title,
            description: newValue.description || null,
            moduleId: newValue.moduleId,
        };

        await this.TestsRepository.update(id, body);
        return this.TestsRepository.findOne(id);
    }
}
