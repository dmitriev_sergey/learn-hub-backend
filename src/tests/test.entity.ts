import { Modules } from 'src/modules/module.entity';
import { Questions } from 'src/questions/question.entity';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne,
    OneToMany,
} from 'typeorm';

@Entity()
export class Tests {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    type: string;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column({ name: 'module_id' })
    moduleId: number;

    @ManyToOne(
        () => Modules,
        modules => modules.tests,
    )
    modules: Modules;

    @OneToMany(
        () => Questions,
        questions => questions.tests,
    )
    questions: Questions[];

		@Column()
    order: number;
}
