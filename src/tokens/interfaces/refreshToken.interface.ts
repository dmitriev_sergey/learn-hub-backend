export interface IRereshToken {
    id: string;
    firstName: string;
    lastName: string;
    phone: string;
    email: string;
    userRole: string;
    iat: number;
    exp: number;
}
