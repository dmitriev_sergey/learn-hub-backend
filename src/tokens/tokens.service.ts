import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as jwt from 'jsonwebtoken';

import { SaveRefreshTokenDto } from './dto/save-refresh-token.dto';
import { RefreshToken } from './entities/refreshToken.entity';
import { IRereshToken } from './interfaces/refreshToken.interface';
import { UserDto } from 'src/users/dto/user-dto';

@Injectable()
export class TokensService {
    constructor(
        @InjectRepository(RefreshToken)
        private readonly refreshTokenRepository: Repository<RefreshToken>,
    ) {}

    generateTokens(user: UserDto) {
        const accessToken = jwt.sign(
            { ...user },
            process.env.JWT_ACCESS_SECRET,
            { expiresIn: process.env.JWT_ACCESS_TOKEN_EXPIRATION },
        );
        const refreshToken = jwt.sign(
            { ...user },
            process.env.JWT_REFRESH_SECRET,
            { expiresIn: process.env.JWT_REFRESH_TOKEN_EXPIRATION },
        );
        return { accessToken, refreshToken };
    }

    public async saveRefreshToken(
        saveRefreshTokenDto: SaveRefreshTokenDto,
    ): Promise<RefreshToken> {
        const { userId, token } = saveRefreshTokenDto;
        try {
            const refreshToken = await this.refreshTokenRepository.findOne({
                userId,
            });
            if (refreshToken) {
                await this.refreshTokenRepository.update(userId, { token });
                return await this.refreshTokenRepository.findOne(userId);
            }
            return await this.refreshTokenRepository.save(saveRefreshTokenDto);
        } catch (err) {
            console.error(
                'Refresh token was not saved, error message:',
                err.message,
            );
        }
    }

    async deleteRefreshToken(refreshToken: string) {
        await this.refreshTokenRepository.delete({ token: refreshToken });
    }

    async findRefreshToken(token: string) {
        try {
            return await this.refreshTokenRepository.findOne({ token });
        } catch (error) {
            console.error(
                'An error occured, tokens.servise, findRefreshTokenById',
                error.message,
            );
            throw error;
        }
    }

    validateRefreshToken(refreshToken: string): IRereshToken {
        try {
            const result = jwt.verify(
                refreshToken,
                process.env.JWT_REFRESH_SECRET,
            );
            return result as IRereshToken;
        } catch (error) {
            console.error(
                'An error occured, tokens.servise, validateRefreshToken',
                error.message,
            );
            return null;
        }
    }
}
