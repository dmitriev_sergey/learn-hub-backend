import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({ name: 'refresh_tokens' })
export class RefreshToken {
    @PrimaryColumn({ name: 'user_id' })
    userId: number;

    @Column()
    token: string;
}
