import { IsString, IsNotEmpty, IsNumber } from 'class-validator';

export class SaveRefreshTokenDto {
    @IsNumber()
    @IsNotEmpty()
    readonly userId: number;

    @IsString()
    @IsNotEmpty()
    readonly token: string;
}
