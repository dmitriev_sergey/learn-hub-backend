import { BeforeInsert, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Roles } from '../enums/role.enum';

@Entity()
export class Users {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'first_name' })
    firstName: string;

    @Column({ name: 'last_name' })
    lastName: string;

    @Column()
    phone: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column({
        name: 'user_role',
        type: 'enum',
        enum: Roles,
        default: Roles.student,
    })
    userRole: Roles;

    @BeforeInsert()
    async hashPassword() {
        try {
            this.password = await bcrypt.hash(this.password, 10);
        } catch (error) {
            console.error('An error occurred while hashing pass', error.message);
        }
    }

    async comparePassword(attempt: string): Promise<boolean> {
        try {
            return await bcrypt.compare(attempt, this.password);
        } catch (error) {
            console.error(
                'An error occurred while comparing pass',
                error.message,
            );
        }
    }

    toResponseObject() {
        const { id, firstName, lastName, email, phone, userRole } = this;
        return {
            id,
            firstName,
            lastName,
            email,
            phone,
            userRole,
        };
    }
}
