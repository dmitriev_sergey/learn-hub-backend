import { Controller, Get, Body, Patch, Delete, Param } from '@nestjs/common';
import { UsersService } from './users.service';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserByIdPipe } from './pipes/user-by-id.pipe';
import { Users } from './entities/user.entity';

@Controller('users')
export class UsersController {
    constructor(private readonly usersService: UsersService) {}

    @Patch(':id')
    update(@Param() params, @Body() updateUserDto: UpdateUserDto) {
        return this.usersService.update(params.id, updateUserDto);
    }

    @Delete(':id')
    remove(@Param() params) {
        return this.usersService.remove(params.id);
    }

    @Get(':id')
    getOne(@Param('id', UserByIdPipe) userEntity: Users) {
        return userEntity;
    }
}
