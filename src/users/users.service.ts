import {
    HttpException,
    HttpStatus,
    Injectable,
    NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UserDto } from './dto/user-dto';
import { Users } from './entities/user.entity';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(Users)
        private usersRepository: Repository<Users>,
    ) {}

    async create(createUserDto: CreateUserDto) {
        return await this.usersRepository.save(createUserDto);
    }

    async findOne(id: number) {
        const user = await this.usersRepository.findOne(id);

        if (!user) {
            throw new NotFoundException(`User with id: ${id} not found`);
        }

        return user;
    }

    async findByEmail(email: string): Promise<Users | null> {
        try {
            return await this.usersRepository.findOne({
                where: { email },
            });
        } catch (error) {
            console.error('error in user.service.findByEmail', error.message);
        }
    }

    async update(id: number, updateUserDto: any) {
        await this.usersRepository.update(id, updateUserDto);
        return this.usersRepository.findOne(id);
    }

    remove(id: number) {
        return this.usersRepository.delete(id);
    }

    public async register(createUserDto: CreateUserDto): Promise<UserDto> {
        const { email } = createUserDto;
        try {
            const userExist = await this.usersRepository.findOne({
                where: { email },
            });

            if (userExist) {
                throw new HttpException(
                    'User already exist',
                    HttpStatus.BAD_REQUEST,
                );
            }

            const userData = await this.usersRepository.create(createUserDto);
            await this.usersRepository.save(userData);
            return new UserDto(userData);
        } catch (error) {
            console.error(
                'An error occurred while using user.service.register',
                error.message,
            );
            throw error;
        }
    }
}
