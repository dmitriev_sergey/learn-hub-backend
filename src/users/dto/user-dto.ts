import { IsEmail, IsString, IsInt, IsNotEmpty } from 'class-validator';

export class UserDto {
    constructor(User) {
        this.id = Number(User.id);
        this.email = User.email;
        this.firstName = User.firstName;
        this.lastName = User.lastName;
        this.phone = User.phone;
        this.userRole = User.userRole;
    }

    @IsString()
    @IsNotEmpty()
    readonly id: number;

    @IsString()
    @IsNotEmpty()
    readonly firstName: string;

    @IsString()
    @IsNotEmpty()
    readonly lastName: string;

    @IsEmail()
    readonly email: string;

    @IsString()
    @IsNotEmpty()
    readonly phone: string;

    @IsString()
    @IsNotEmpty()
    readonly password: string;

    @IsInt()
    @IsNotEmpty()
    readonly userRole: string;
}
