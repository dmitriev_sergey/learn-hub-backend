export enum Roles {
    student = 'student',
    teacher = 'teacher',
    admin = 'admin',
}
