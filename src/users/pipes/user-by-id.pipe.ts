import { PipeTransform, Injectable } from '@nestjs/common';
import { Users } from '../entities/user.entity';
import { UsersService } from '../users.service';

@Injectable()
export class UserByIdPipe implements PipeTransform<number, Promise<Users>> {
    constructor(private readonly usersService: UsersService) {}

    transform(id: number): Promise<Users> {
        return this.usersService.findOne(id);
    }
}
