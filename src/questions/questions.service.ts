import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Like, Repository } from 'typeorm';

import { Questions } from './question.entity';
import { getSkipCount } from '../utils/selection';
import { DEFAULT_PAGE, DEFAULT_SIZE } from '../constants/common';
import { CreateQuestionDto } from './dto/create-question.dto';
import { UpdateQuestionDto } from './dto/update-question.dto';

@Injectable()
export class QuestionsService {
    constructor(
        @InjectRepository(Questions)
        private readonly questionRepository: Repository<Questions>,
    ) {}

    async findAll(query): Promise<Questions[]> {
        const { page = DEFAULT_PAGE, size = DEFAULT_SIZE, search = '' } = query;
        const skip = getSkipCount(page, size);
        return await this.questionRepository.find({
            where: {
                title: Like(`%${search}%`),
            },
            skip,
            take: size,
        });
    }

    async findOne(id: number): Promise<Questions> {
        return await this.questionRepository.findOne(id);
    }

    async createQuestion(
        createQuestionDto: CreateQuestionDto,
    ): Promise<Questions> {
        const body = {
            type: createQuestionDto.type,
            title: createQuestionDto.title,
            description: createQuestionDto.description,
            test: createQuestionDto.test,
            options: createQuestionDto.options,
            correctOptions: createQuestionDto.correctOptions,
						order: createQuestionDto.order
        };
        try {
            return await this.questionRepository.save(body);
        } catch (err) {
            console.error(
                'Questions was not created, error message:',
                err.message,
            );
        }
    }

    async deleteQuestion(id: number): Promise<DeleteResult> {
        const question = await this.questionRepository.findOne(id);
        return await this.questionRepository.delete(question);
    }

    async updateQuestion(
        id: number,
        updateQuestionDto: UpdateQuestionDto,
    ): Promise<Questions> {
        const question = await this.questionRepository.findOneOrFail(id);

        if (!question) {
            console.log('Question was not found');
            return null;
        }

        const body = {
            type: updateQuestionDto.type,
            title: updateQuestionDto.title,
            description: updateQuestionDto.description,
            test: updateQuestionDto.test,
            options: updateQuestionDto.options,
            correctOptions: updateQuestionDto.correctOptions,
        };

        await this.questionRepository.update(id, body);
        return this.questionRepository.findOne(id);
    }
}
