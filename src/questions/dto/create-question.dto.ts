import { IsString, IsInt, IsNotEmpty } from 'class-validator';

export class CreateQuestionDto {
    @IsInt()
    @IsNotEmpty()
    readonly test: number;

		@IsInt()
    @IsNotEmpty()
    readonly order: number;

    @IsString()
    @IsNotEmpty()
    readonly title: string;

    @IsString()
    @IsNotEmpty()
    readonly type: string;

    @IsString()
    @IsNotEmpty()
    readonly description: string;

    @IsNotEmpty()
    readonly options: string;

    @IsNotEmpty()
    readonly correctOptions: string;
}
