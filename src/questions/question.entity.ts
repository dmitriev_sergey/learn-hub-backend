import { Tests } from 'src/tests/test.entity';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

@Entity({ name: 'questions' })
export class Questions {
    @PrimaryGeneratedColumn()
    id: number;

		@Column()
		order: number;

    @Column({ name: 'test_id' })
    test: number;

    @Column()
    title: string;

    @Column()
    type: string;

    @Column()
    description: string;

    @Column({ type: 'jsonb' })
    options: string;

    @Column({ name: 'correct_options', type: 'jsonb' })
    correctOptions: string;

    @ManyToOne(
        () => Tests,
        tests => tests.questions,
    )
    tests: Tests;
}
