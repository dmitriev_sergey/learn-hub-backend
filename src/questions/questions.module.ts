import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Questions } from './question.entity';
import { QuestionsController } from './questions.controller';
import { QuestionsService } from './questions.service';

@Module({
    imports: [TypeOrmModule.forFeature([Questions])],
    controllers: [QuestionsController],
    providers: [QuestionsService],
})
export class QuestionsModule {}
