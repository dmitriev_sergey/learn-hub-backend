import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    Query,
} from '@nestjs/common';
import { DeleteResult } from 'typeorm';
import { Questions } from './question.entity';
import { QuestionsService } from './questions.service';
import { CreateQuestionDto } from './dto/create-question.dto';
import { UpdateQuestionDto } from './dto/update-question.dto';

@Controller('questions')
export class QuestionsController {
    constructor(private readonly questionsService: QuestionsService) {}

    @Get()
    findAll(@Query() query): Promise<Questions[]> {
        return this.questionsService.findAll(query);
    }

    @Get(':id')
    findOne(@Param() params): Promise<Questions> {
        return this.questionsService.findOne(params.id);
    }

    @Post()
    createOne(
        @Body() createQuestionDto: CreateQuestionDto,
    ): Promise<Questions> {
      return this.questionsService.createQuestion(createQuestionDto);
    }

    @Delete(':id')
    deleteOne(@Param() params): Promise<DeleteResult> {
        return this.questionsService.deleteQuestion(params.id);
    }

    @Patch(':id')
    updateOne(
        @Param() params,
        @Body() updateQuestionDto: UpdateQuestionDto,
    ): Promise<Questions> {
        return this.questionsService.updateQuestion(
            params.id,
            updateQuestionDto,
        );
    }
}
