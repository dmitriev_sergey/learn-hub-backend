import { DEFAULT_PAGE } from '../constants/common';

export const getSkipCount = (page: number, size: number): number =>
    page !== DEFAULT_PAGE && (page - 1) * size;
