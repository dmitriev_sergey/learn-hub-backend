import { Courses } from 'src/courses/course.entity';
import { Tests } from 'src/tests/test.entity';
import { Theories } from 'src/theories/theory.entity';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne,
    OneToMany,
} from 'typeorm';

@Entity()
export class Modules {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column({ name: 'course_id' })
    courseId: number;

    @ManyToOne(
        () => Courses,
        course => course.modules,
    )
    course: Courses;

    @OneToMany(
        () => Tests,
        tests => tests.modules,
    )
    tests: Tests[];

    @OneToMany(
        () => Theories,
        theories => theories.modules,
    )
    theories: Theories[];

		@Column()
    order: number;
}
