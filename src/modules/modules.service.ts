import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DEFAULT_PAGE, DEFAULT_SIZE } from '../constants/common';
import { getSkipCount } from '../utils/selection';
import { Repository, Equal, Not, IsNull, Like, DeleteResult } from 'typeorm';
import { CreateModuleDto } from './dto/create-module.dto';
import { Modules } from './module.entity';
import { UpdateModuleDto } from './dto/update-module.dto';

@Injectable()
export class ModulesService {
    constructor(
        @InjectRepository(Modules)
        private readonly moduleRepository: Repository<Modules>,
    ) {}

    async findAll(query): Promise<Modules[]> {
        const {
            page = DEFAULT_PAGE,
            size = DEFAULT_SIZE,
            search = '',
            course,
        } = query;
        const skip = getSkipCount(page, size);
        const course_id = course ? Equal(course) : Not(IsNull());
        return await this.moduleRepository.find({
            where: {
                title: Like(`%${search}%`),
                course_id,
            },
            skip,
            take: size,
        });
    }

    async findOne(id: number): Promise<Modules> {
        return await this.moduleRepository.findOne(id);
    }

    async createModule(data: CreateModuleDto): Promise<Modules> {
        const body = {
            title: data.title,
            description: data.description || null,
            courseId: data.courseId,
						order: data.order
        };
        try {
            return await this.moduleRepository.save(body);
        } catch (err) {
            console.error(
                'Module was not created, error message:',
                err.message,
            );
        }
    }

    async deleteModule(id: number): Promise<DeleteResult> {
        const course = await this.moduleRepository.findOne(id);
        return await this.moduleRepository.delete(course);
    }

    async updateModule(id: number, data: UpdateModuleDto): Promise<Modules> {
        const course = await this.moduleRepository.findOneOrFail(id);
        if (!course) {
            console.log('Module was not found');
        }

        const body = {
            title: data.title,
            description: data.description || null,
            courseId: data.courseId,
						order: data.order
        };

        await this.moduleRepository.update(id, body);
        return this.moduleRepository.findOne(id);
    }
}
