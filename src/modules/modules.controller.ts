import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    Query,
} from '@nestjs/common';
import { DeleteResult } from 'typeorm';
import { CreateModuleDto } from './dto/create-module.dto';
import { Modules } from './module.entity';
import { ModulesService } from './modules.service';
import { UpdateModuleDto } from './dto/update-module.dto';

@Controller('modules')
export class ModulesController {
    constructor(private readonly modulesService: ModulesService) {}

    @Get()
    findAll(@Query() query): Promise<Modules[]> {
        return this.modulesService.findAll(query);
    }

    @Get(':id')
    findOne(@Param() params): Promise<Modules> {
        return this.modulesService.findOne(params.id);
    }

    @Post()
    createOne(@Body() createModuleDto: CreateModuleDto): Promise<Modules> {
        return this.modulesService.createModule(createModuleDto);
    }

    @Delete(':id')
    deleteOne(@Param() params): Promise<DeleteResult> {
        return this.modulesService.deleteModule(params.id);
    }

    @Patch(':id')
    updateOne(
        @Param() params,
        @Body() updateModuleDto: UpdateModuleDto,
    ): Promise<Modules> {
        return this.modulesService.updateModule(params.id, updateModuleDto);
    }
}
