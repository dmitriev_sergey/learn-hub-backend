import { IsString, IsInt, IsNotEmpty } from 'class-validator';

export class CreateModuleDto {
    @IsString()
    @IsNotEmpty()
    readonly title: string;

    @IsString()
    @IsNotEmpty()
    readonly description: string;

    @IsInt()
    @IsNotEmpty()
    readonly courseId: number;

		@IsInt()
    @IsNotEmpty()
    readonly order: number;
}
