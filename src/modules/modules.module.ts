import { Module } from '@nestjs/common';
import { ModulesService } from './modules.service';
import { ModulesController } from './modules.controller';
import { Modules } from './module.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
    imports: [TypeOrmModule.forFeature([Modules])],
    providers: [ModulesService],
    controllers: [ModulesController],
})
export class ModulesModule {}
