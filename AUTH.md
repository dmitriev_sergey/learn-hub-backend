### Refresh
* User have been already authenticated
* AccessToken have been stored in localStorage
* RefreshToken have been set to cookies
* Client's axios instace has param withCredentials=true

1. After every page refresh/ rendering app call api/refresh, where cookies includes Header "refreshToken".
2. BE should validate refreshToken.
	2.1 if VALID, generate new tokens, get userInfo, return an object with tokens and userInfo
	2.2 if NOT VALID, return 401
	